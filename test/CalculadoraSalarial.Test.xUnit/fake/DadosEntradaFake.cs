﻿using Bogus;
using CalculadoraSalarial.Test.xUnit.dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalculadoraSalarial.Test.xUnit.fake
{
    //TODO: classe exemplo de fake.
    public class DadosEntradaFake
    {
        public BaseEntradaSalario FakerEntradaSalario()
        {
            return new Faker<BaseEntradaSalario>("pt_BR")
                .RuleFor(c => c.salarioBase, f => f.Random.Double())
                .RuleFor(c => c.numeroDependentes, f => f.Random.Int())
                .RuleFor(c => c.demaisDescontos, f => f.Random.Double())
                .Generate();
        }
    }
}

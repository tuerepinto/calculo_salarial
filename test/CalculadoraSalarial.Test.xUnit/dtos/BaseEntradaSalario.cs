﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculadoraSalarial.Test.xUnit.dtos
{
    public class BaseEntradaSalario
    {
        public double salarioBase { get; set; }
        public int? numeroDependentes { get; set; }
        public double demaisDescontos { get; set; }
    }
}

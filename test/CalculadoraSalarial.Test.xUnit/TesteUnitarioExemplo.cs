using System;
using Xunit;

namespace CalculadoraSalarial.Test.xUnit
{
    public class TesteUnitarioExemplo
    {
        [Fact]
        public void Test1()
        {
            // Arrange
            
            // Act
            
            // Assert
        }

        [Theory]
        [InlineData(1, 2, 3)]
        [InlineData(-4, -6, -10)]
        [InlineData(-2, 2, 0)]
        public void Test2(double salarioBase, int? numeroDependentes, double demaisDescontos)
        {
            // Arrange

            // Act

            // Assert
        }
    }
}
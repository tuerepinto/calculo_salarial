﻿using CalculadoraSalarial.Test.xUnit.dtos;
using CalculadoraSalarial.Test.xUnit.fake;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalculadoraSalarial.Test.xUnit.mock
{
    //TODO: class
    public class DadosEntradaMock
    {
        public Mock<BaseEntradaSalario> MockEntradaSalario()
        {
            var mock = new Mock<BaseEntradaSalario>();

            mock.Setup(m => m.salarioBase).Returns(10.0);

            return mock;
        }
    }
}

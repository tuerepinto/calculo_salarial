﻿using CalculadoraSalarial.Domain.Interfaces.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalculadoraSalarial.Test.xUnit.fixture
{
    class SalarioLiquidoFixture
    {
        private readonly ISalarioLiquido salarioLiquido;
        public SalarioLiquidoFixture()
        {
            var serviceMock = new Mock<ISalarioLiquido>();
            serviceMock.Setup(x => x.ObterValorSalarioLiquido(It.IsAny<double>(), It.IsAny<int>(), It.IsAny<double>())).Returns(0.0);            
        }

        //TODO: create faker mock date;
    }
}

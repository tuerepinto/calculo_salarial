﻿using CalculadoraSalarial.Domain.Interfaces.Business;
using CalculadoraSalarial.Domain.Interfaces.Services;

namespace CalculadoraSalarial.UseCase
{
    public class SalarioLiquido : ISalarioLiquido
    {
        private readonly ICalculadora _calculadora;
        public SalarioLiquido(ICalculadora calculadora)
        {
            _calculadora = calculadora;
        }

        public double ObterValorSalarioLiquido(double salarioBase, int? numeroDependentes, double demaisDescontos)
        {
             return _calculadora.ObterValorSalarioLiquido(salarioBase, numeroDependentes, demaisDescontos);
        }
    }
}
﻿using CalculadoraSalarial.Domain.Interfaces.Business;
using CalculadoraSalarial.Domain.Interfaces.Repository;

namespace CalculadoraSalarial.Business
{
    public class Calculadora : ICalculadora
    {
        private readonly ITabelaIRRF _irrf;
        private readonly ITabelaINSS _inss;

        private const double ValorDependente = 189.59;

        public Calculadora(ITabelaIRRF irrf, ITabelaINSS inss)
        {
            _irrf = irrf;
            _inss = inss;
        }

        //TODO: Salário líquido
        public double ObterValorSalarioLiquido(double salarioBase, int? numeroDependentes, double demaisDescontos)
        {
            var irrfComDependente = 0.0;
            var irrfSemDependente = 0.0;

            var valorDescontoInss = (double) _inss.ObterValorTabelaINSS(salarioBase);
            var salarioDescontoInss = salarioBase - valorDescontoInss;

            if (numeroDependentes != null && numeroDependentes > 0)
            {
                irrfComDependente = DeducaoIRRFComDependente(numeroDependentes, salarioDescontoInss);
            }
            else
            {
                irrfSemDependente =
                    DeducaoIRRFSemDependente(salarioDescontoInss *
                                             (double) _irrf.ObterValorTabelaIRRF(salarioBase).Percentual /
                                             100.00, salarioBase);
            }

            var descontoTotal = valorDescontoInss
                                + (numeroDependentes != null ? irrfComDependente : irrfSemDependente)
                                + demaisDescontos;

            return salarioBase - descontoTotal;
        }

        private double DeducaoIRRFComDependente(int? numeroDependentes, double salarioDescontoInss)
        {
            var result = 0.0;
            var soma = 0.0;

            for (int i = 0; i < numeroDependentes; i++)
            {
                soma = numeroDependentes.Value * ValorDependente;
            }

            result = salarioDescontoInss - soma;

            return result;
        }

        private double DeducaoIRRFSemDependente(double valorIrrfPercentual, double salarioBase)
        {
            var result = 0.0;

            result = valorIrrfPercentual - _irrf.ObterValorTabelaIRRF(salarioBase).Decucao;

            return result;
        }
    }
}
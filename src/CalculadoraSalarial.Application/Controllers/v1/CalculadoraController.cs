﻿using CalculadoraSalarial.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace CalculadoraSalarial.Application.Controllers.v1
{
    [ApiController]
    [ApiVersion("1.0", Deprecated = false)]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CalculadoraController : Controller
    {
        private readonly ISalarioLiquido _salarioLiquido;
        public CalculadoraController(ISalarioLiquido salarioLiquido)
        {
            _salarioLiquido = salarioLiquido;
        }
        
        [HttpGet()]
        public ActionResult GetValorSalarialMensal()
        {
            return null;
        }
    }
}
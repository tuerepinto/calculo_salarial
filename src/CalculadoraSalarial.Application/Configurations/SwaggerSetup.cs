﻿using System.ComponentModel;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace CalculadoraSalarial.Application.Configurations
{
    public static class SwaggerSetup
    {
        public static void AddSwaggerConfiguration(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(opt =>
            {
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    opt.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = "Calculadora Salarial",
                        Version = description.ApiVersion.ToString(),
                        Description = "",
                    });
                }
            });
        }
    }
}

﻿using CalculadoraSalarial.Business;
using CalculadoraSalarial.Domain.Interfaces.Business;
using CalculadoraSalarial.Domain.Interfaces.Repository;
using CalculadoraSalarial.Domain.Interfaces.Services;
using CalculadoraSalarial.Repository;
using CalculadoraSalarial.UseCase;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace CalculadoraSalarial.Application.DependencyInjection
{
    public static class DependencyInjectionSetup
    {
        //TODO: read doc microsot https://docs.microsoft.com/pt-br/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-3.1
        public static void AddDependencyInjection(this IServiceCollection services)
        {
            #region injection singleton

            //  services.AddSingleton<ISalarioLiquido, SalarioLiquido>();

            #endregion

            #region injection scoped

            services.TryAddScoped<ITabelaINSS, TabelaINSS>();
            services.TryAddScoped<ITabelaIRRF, TabelaIRRF>();
            services.TryAddScoped<ICalculadora, Calculadora>();

            #endregion

            #region ingection

            // services.AddTransient<ICalculadora, Calculadora>();

            #endregion
        }
    }
}
﻿namespace CalculadoraSalarial.Domain.Interfaces.Business
{
    public interface ICalculadora
    {
        double ObterValorSalarioLiquido(double salarioBase, int? numeroDependentes, double demaisDescontos);
    }
}
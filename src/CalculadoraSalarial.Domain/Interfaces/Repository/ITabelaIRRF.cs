﻿using CalculadoraSalarial.Domain.Dtos;

namespace CalculadoraSalarial.Domain.Interfaces.Repository
{
    public interface ITabelaIRRF
    {
        IRRF ObterValorTabelaIRRF(double salarioBase);
    }
}
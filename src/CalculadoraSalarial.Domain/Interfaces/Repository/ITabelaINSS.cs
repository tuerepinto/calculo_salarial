﻿namespace CalculadoraSalarial.Domain.Interfaces.Repository
{
    public interface ITabelaINSS
    {
        decimal ObterValorTabelaINSS(double salarioBase);
    }
}
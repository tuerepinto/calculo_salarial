﻿namespace CalculadoraSalarial.Domain.Interfaces.Services
{
    public interface ISalarioLiquido
    {
        double ObterValorSalarioLiquido(double salarioBase, int? numeroDependentes, double demaisDescontos);
    }
}
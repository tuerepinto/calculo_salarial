﻿namespace CalculadoraSalarial.Domain.Dtos
{
    //TODO: Dedução por dependente na determinação da base de cálculo do IRRF - 189,59
    public class IRRF
    {
        public double Percentual { get; set; }
        public double Decucao { get; set; }
    }
}
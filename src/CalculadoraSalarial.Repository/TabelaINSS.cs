﻿using CalculadoraSalarial.Domain.Interfaces.Repository;

namespace CalculadoraSalarial.Repository
{
    public class TabelaINSS : ITabelaINSS
    {
        private const double SalarioMinimo = 1751.81;
        private const double SalarioMedio = 2919.72;
        private const double SalarioMaximo = 5839.45;

        public decimal ObterValorTabelaINSS(double salarioBase)
        {
            var result = 0m;
            if (salarioBase <= SalarioMinimo)
            {
                result = 8;
            }

            if (salarioBase >= (SalarioMinimo + 0.01) && salarioBase <= SalarioMedio)
            {
                result = 9;
            }

            if (salarioBase >= (SalarioMinimo + 0.01) && salarioBase <= SalarioMaximo)
            {
                result = 11;
            }

            if (salarioBase >= (SalarioMaximo + 0.01))
            {
                result = 642.34m;
            }

            return result;
        }
    }
}
﻿using CalculadoraSalarial.Domain.Dtos;
using CalculadoraSalarial.Domain.Interfaces.Repository;

namespace CalculadoraSalarial.Repository
{
    public class TabelaIRRF : ITabelaIRRF
    {
        private const double SalarioMinimoIRRF = 1903.98;
        private const double SalarioMedioIRRF = 2826.65;
        private const double SalarioMedioDoisIRRF = 3751.05;
        private const double SalarioMaximoIRRF = 4664.68;

        public IRRF ObterValorTabelaIRRF(double salarioBase)
        {
            var resultIRRF = new IRRF();
            if (salarioBase <= SalarioMinimoIRRF)
            {
                resultIRRF.Percentual = 0;
                resultIRRF.Decucao = 0;
            }

            if (salarioBase >= (SalarioMinimoIRRF + 0.01) && salarioBase <= SalarioMedioIRRF)
            {
                resultIRRF.Percentual = 7.50;
                resultIRRF.Decucao = 142.80;
            }

            if (salarioBase >= (SalarioMedioIRRF + 0.01) && salarioBase <= SalarioMedioDoisIRRF)
            {
                resultIRRF.Percentual = 15;
                resultIRRF.Decucao = 354.80;
            }

            if (salarioBase >= (SalarioMedioDoisIRRF + 0.01) && salarioBase <= SalarioMaximoIRRF)
            {
                resultIRRF.Percentual = 22.5;
                resultIRRF.Decucao = 636.13;
            }

            if (salarioBase >= (SalarioMaximoIRRF + 0.01))
            {
                resultIRRF.Percentual = 27.5;
                resultIRRF.Decucao = 869.36;
            }

            return resultIRRF;
        }
    }
}